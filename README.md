# Salvage

## The Problem

One of my other projects, [Minecraft Manager](https://github.com/J3RN/minecraft-manager) creates a ton of DigitalOcean snapshots. This was fine at the time it was designed, as snapshots could be stored on DigitalOcean for free. This is no longer true, and it's costing me a ton to keep all of those snapshots. Hopefully in the future, Minecraft Manager will just make a tar the minecraft world and upload it to a service such as AWS' S3.

## The Solution

Salvage, to the rescue! Salvage converts those old snapshots into tars of my minecraft worlds, which it then downloads. From there, I can move them to S3 myself, or just keep them locally. They're just tar files; I can do anything with them.

## Prerequisites

This script assumes you have Ruby 2.5.0 installed (as specified in the `.ruby-version` file). It may work with earlier Ruby versions; but if so it is merely an accident.

## Usage

1. Clone the repository

    ```bash
    git clone git@gitlab.com:J3RN/salvage
    ```
2. Install the dependencies

    ```bash
    bundle
    ```
3. Run the program. It will prompt you for all additional information.

    ```bash
    ruby salvage.rb
    ```
