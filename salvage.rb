require 'droplet_kit'
require 'sshkit'
require 'sshkit/dsl'

class Salvager
  def initialize
    @client = DropletKit::Client.new(access_token: get_token)
    @ssh_key = select_ssh_key
    @images = select_images
  end

  def salvage
    @images.each do |image|
      droplet = start_droplet(image)
      destroy_image(image) if Downloader.new(@client, droplet).download_world
      stop_droplet(droplet)
    end
  end

  private

  def select_ssh_key
    @client.ssh_keys.all.detect do |key|
      get_response("Do you want to use this key: #{key.name}?")[0] == 'y'
    end
  end

  def select_images
    @client.images.all.
      reject(&:public).
      select do |image|
      get_response("Do you want to salvage \"#{image.name}\"?")[0] == 'y'
    end
  end

  def destroy_image(image)
    @client.images.delete(id: image.id)
  end

  def get_token
    get_response("What is your DigitalOcean access token?")
  end

  def start_droplet(image)
    @client.droplets.create(DropletKit::Droplet.new(
      name: SecureRandom.hex(10),
      region: 'nyc3',
      image: image.id,
      size: '4gb',
      ssh_keys: [@ssh_key.id]
    ))
  end

  def stop_droplet(droplet)
    @client.droplets.delete(id: droplet.id)
  end

  def get_response(message)
    puts message
    print "> "
    gets.chomp
  end
end

class Downloader
  include SSHKit::DSL

  def initialize(client, droplet)
    @client = client
    @droplet = droplet
  end

  def download_world
    name = @droplet.image.name

    # Wait for server to come online
    while @client.droplets.find(id: @droplet.id).status != 'active' do
      puts "Waiting for #{name} to wake from slumber..."
      sleep 60
    end

    on "root@#{@client.droplets.find(id: @droplet.id).public_ip}" do
      within '/home/j3rn' do
        # Stop server
        execute(:systemctl, 'stop', 'minecraft')
        # Compress world
        execute(:tar, '-cf', 'minecraft.tar.gz', 'minecraft')
        # Download that sucker
        download! 'minecraft.tar.gz', "downloads/#{name}.tar.gz"
      end
    end

    true
  rescue
    false
  end
end

Salvager.new.salvage
